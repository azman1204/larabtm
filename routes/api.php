<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// return data ticket dlm bentuk JSON = Javascript Object Notation
// format json : [] = array dan {} = obj 
// contoh : [{"nama":"azman", "alamat":"Bangi"}, {"nama":"John Doe", "alamat":"London"}] 
Route::get('/ticket', function() {
    return \App\Ticket::all();
});

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
