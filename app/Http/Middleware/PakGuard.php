<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class PakGuard
{
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            // user dah login, boleh kemana mana shj
            return $next($request);
        } else {
            // user belum login, redirect ke welcome pg.
            return redirect('/');
        }
    }
}
