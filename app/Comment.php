<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model {
    protected $table = 'comment';
    public $timestamps = false; // bg tau table kita tak ada col. created_at dan updated_at

    // relationship dengan Ticket
    public function ticket() {
        return $this->belongsTo(App\Ticket::class);
    }
}